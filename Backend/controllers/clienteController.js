const Cliente = require("../models/Cliente");

exports.crearCliente = async (req, res) => {
    try {
        let cliente;
        // creamos nuestro cliente
        cliente = new Cliente(req.body);
        await cliente.save();
        res.send(cliente);
    } catch (error) {
        console.log(error);
        res.status(500).send("hay un error al recibir los datos");
    }
};

exports.mostrarClientes = async (req, res) => {
    try {
        const clientes = await Cliente.find();
        res.json(clientes);
    } catch (error) {
        console.log(error);
        res.status(500).send("hay un error al recibir los datos");
    }
};

exports.obtenerCliente = async (req, res) => {
    try {
        let cliente = await cliente.findById(req.params.id);
        if (!cliente) {
            res.status(404).json({ msg: "el producto no existe" });
        }
        res.json(cliente);
    } catch (error) {
        console.log(error);
        res.status(500).send("hay un error al recibir los datos");
    }
};

exports.eliminarCliente = async (req, res) => {
    try {
        let cliente = await Cliente.findById(req.params.id);
        if (!cliente) {
            res.status(404).json({ msg: "el cliente no existe" });
        }
        await Cliente.findByIdAndRemove({ _id: req.params.id });
        res.json({ msg: "cliente eliminado con exito" });
    } catch (error) {
        console.log(error);
        res.status(500).send("hay un error al recibir los datos");
    }
};

exports.actualizarCliente = async (req, res) => {
    try {
        const { nombreCliente, edad, direccion, telefono } = req.body;
        let cliente = await Cliente.findById(req.params.id);
        if (!cliente) {
            res.status(404).json({ msg: "el cliente no existe" });
        }
        cliente.nombreCliente = nombreCliente;
        cliente.edad = edad;
        cliente.direccion = direccion;
        cliente.telefono = telefono;

        cliente = await Cliente.findOneAndUpdate({ _id: req.params.id }, cliente, {
        new: true,
        });
        res.json(cliente);
    } catch (error) {
        console.log(error);
        res.status(500).send("hay un error al recibir los datos, actualizar");
    }
};
