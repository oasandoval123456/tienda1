const mongoose = require('mongoose');

const clienteSchema = mongoose.Schema({
    nombreCliente:{
        type: String,
        required: true
    },
    edad:{
        type: Number,
        required: true
    },
    direccion:{
        type: String,
        required: true
    },
    telefono:{
        type: String,
        required: true
    }
});

module.exports = mongoose.model('clientes', clienteSchema );